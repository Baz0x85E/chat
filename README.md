# Chat

The project Chat has client-server architecture and using sockets to communicate.

This project is intended to show my professional skill:
* Using of C++
* Writing a clean code
* Using different libraries and frameworks like posix, protobuf, boost, google tests
* Multithreading programming
* Network programming
* Handling OS signals and printing nice stack traces with function names

Project highlight:
* Added StopToken to the most of blocking operations -- prevents app freezing in case of closing
* Server and client can be stopped by CTRL+C due to signal handling and using of StopToken
* Sockets: using own wrappers for posix sockets
* Protobuf: using to encode and decode messages
* Signals: using Boost::Signals2 for async programming
* Logger: using Boost::Log, added thread names printing 
* Stack trace: using nice stack trace with function names like in Java, C#, Python, etc
* Exceptions: using exeptions, which allow to print nice stack trace
* OS signals handling
