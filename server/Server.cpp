#include "server/Server.hpp"

#include "common/Logger.hpp"
#include "common/protocol/Helpers.hpp"

#include <algorithm>

namespace server
{

	namespace
	{

		const auto ClientInitTimeout = std::chrono::seconds(30);
		const auto TaskConditionSleepTimeout = std::chrono::minutes(1);

	}


	Server::Server(const Url& address)
		:	_listener(address)
	{
		_connections.emplace_back(connectAsTask(_listener.onConnection(), std::bind(&Server::onListenerConnectionHandler, this, std::placeholders::_1)));
		_connections.emplace_back(connectAsTask(_listener.onError(), std::bind(&Server::onListenerErrorHandler, this)));
	}


	void Server::start()
	{
		ActionLogger al("Server::start");

		while (!_stopSource.isStopped())
		{
			{
				std::unique_lock l(_tasksMutex);
				_tasksCondition.wait_for(l, TaskConditionSleepTimeout, [this]{ return _stopSource.isStopped() || !_tasks.empty(); });
			}

			if (_stopSource.isStopped())
				break;

			for (auto it = _uninitialisedClients.begin(); it != _uninitialisedClients.end(); )
			{
				if (it->second.second + ClientInitTimeout < std::chrono::steady_clock::now())
				{
					logEvent(LogLevel::Debug, boost::format("Server::start: erasing uninitialised client: %1% ") % it->first.toString());
					it = _uninitialisedClients.erase(it);
				}
				else
					++it;
			}

			auto localTasks = [this]
			{
				std::lock_guard l(_tasksMutex);
				return std::exchange(_tasks, {});
			}();

			while (!localTasks.empty())
			{
				Task task = localTasks.front();
				localTasks.pop();

				try
				{ task(); }
				catch (const std::exception& ex)
				{ logEvent(LogLevel::Error, boost::format("Server::start: task: %1%") % ex.what()); }
			}
		}
	}


	void Server::requestStop()
	{
		_stopSource.stop();
		_tasksCondition.notify_one();
	}


	void Server::addTask(const Task& task)
	{
		std::lock_guard l(_tasksMutex);
		_tasks.push(task);
		_tasksCondition.notify_one();
	}


	void Server::onListenerConnectionHandler(const std::shared_ptr<protocol::ServerSideConnection>& connection)
	{
		logEvent(LogLevel::Info, boost::format("A new connection from: %1%") % connection->getEndpointAddress().toString());

		const auto client = std::make_shared<Client>(connection);

		SignalConnections signalConnections;
		signalConnections.emplace_back(
				connectAsTask(client->onInit(), std::bind(&Server::onInitHandler, this, connection->getEndpointAddress(), std::placeholders::_1)));

		_uninitialisedClients.emplace(
				connection->getEndpointAddress(),
				std::make_pair(ClientInfo(client, std::move(signalConnections)), std::chrono::steady_clock::now()));
	}


	void Server::onListenerErrorHandler()
	{
		logEvent(LogLevel::Error, "Server::onErrorHandler: Error!");
		_stopSource.stop();
	}


	void Server::onInitHandler(const Url& clientAddress, const protocol::ClientMessage::Init& message)
	{
		auto uninitialisedClientIt = _uninitialisedClients.find(clientAddress);
		if (uninitialisedClientIt == _uninitialisedClients.end())
			return;

		const ClientPtr sender = uninitialisedClientIt->second.first.ptr;
		_uninitialisedClients.erase(uninitialisedClientIt);

		logEvent(LogLevel::Debug, boost::format("Init from: %1%") % message.name());

		const bool nameIsAvailable = _clients.count(message.name()) == 0;

		if (nameIsAvailable)
		{
			SignalConnections connections;
			connections.emplace_back(connectAsTask(sender->onMessage(), std::bind(&Server::onMessageHandler, this, message.name(), std::placeholders::_1)));
			connections.emplace_back(connectAsTask(sender->onDisconnected(), std::bind(&Server::onClientDisconnectedHandler, this, message.name())));

			const auto request = protocol::makeServerRequestNewClient(message.name());

			try
			{
				for (const auto& [_, client] : _clients)
					client.ptr->enqueueMessage(request);
			}
			catch (const std::exception& ex)
			{ logEvent(LogLevel::Error, boost::format("Server::onInitHandler: %1%") % ex.what()); }

			_clients.emplace(message.name(), ClientInfo(sender, std::move(connections)));
		}

		sender->enqueueMessage(protocol::makeServerRequestInitResult(nameIsAvailable,
				(nameIsAvailable ? boost::format("Welcome, %1%") % message.name() : boost::format("Name %1% is not available") % message.name()).str()));
	}


	void Server::onMessageHandler(const std::string& senderName, const protocol::ClientMessage::SendMessage& message)
	{
		logEvent(LogLevel::Debug, boost::format("Message from: %1%: %2%") % senderName % message.message());

		const auto request = protocol::makeServerRequestClientSentMessage(
				senderName,
				message.message(),
				std::chrono::steady_clock::now().time_since_epoch().count());

		for (const auto& [name, client] : _clients)
		{
			if (name != senderName)
				client.ptr->enqueueMessage(request);
		}
	}


	void Server::onClientDisconnectedHandler(const std::string& senderName)
	{
		logEvent(LogLevel::Info, boost::format("Client with name: '%1%' disconnected") % senderName);

		_clients.erase(senderName);

		const auto request = protocol::makeServerRequestClientDisconnected(senderName);

		for (const auto& [_ ,client] : _clients)
		{ client.ptr->enqueueMessage(request); }
	}

}
