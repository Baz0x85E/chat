#pragma once

#include "common/protocol/ServerSideConnection.hpp"

namespace server
{

	class Client
	{
	public:
		template<class Signature>
		using Signal = boost::signals2::signal<Signature>;

		using OnInitSignal = Signal<void(const protocol::ClientMessage::Init&)>;
		using OnMessageSignal = Signal<void(const protocol::ClientMessage::SendMessage&)>;
		using OnDisconnectedSignal = Signal<void()>;

	private:
		std::shared_ptr<protocol::ServerSideConnection>	_connection;

		std::string										_name;

		StopToken										_stopSource;

		OnInitSignal									_onInit;
		OnMessageSignal									_onMessage;
		OnDisconnectedSignal							_onDisconnected;

		std::vector<boost::signals2::connection>		_connections;

	public:
		explicit Client(std::shared_ptr<protocol::ServerSideConnection> connection);
		~Client();

		const std::string& getName() const noexcept;

		void enqueueMessage(const std::shared_ptr<protocol::BaseMessage>& message);

		void disconnect();

		OnInitSignal& onInit()
		{ return _onInit; }

		OnMessageSignal& onMessage()
		{ return _onMessage; }

		OnDisconnectedSignal& onDisconnected()
		{ return _onDisconnected; }

	private:
		void onRequestHandler(const std::shared_ptr<protocol::Request>& request);
		void onErrorHandler();

		void receiveWorkerLoop(const IStopToken& token) noexcept;
		void sendWorkerLoop(const IStopToken& token) noexcept;
	};

}
