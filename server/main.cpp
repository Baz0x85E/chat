#include "common/Logger.hpp"
#include "common/Thread.hpp"
#include "server/Server.hpp"

#ifdef DEBUG_MODE
	#undef BACKWARD_HAS_BFD
	#define BACKWARD_HAS_BFD 1
	#include <backward.hpp>
#endif

#include <boost/format.hpp>

#include <csignal>
#include <iostream>
#include <memory>

namespace
{
#ifdef DEBUG_MODE
	backward::SignalHandling sh;
#endif

	server::Server* serverPtr = nullptr;

	void sigtermHandler(int)
	{
		if (serverPtr)
			serverPtr->requestStop();
	}
}


int main(int argc, char *argv[])
{
	unsigned port = 8091;

	std::cout << "Enter the server's port: ";
	std::cin >> port;

	std::signal(SIGTERM, sigtermHandler);
	std::signal(SIGINT, sigtermHandler);

	try
	{
		initLogger(LogLevel::Debug, "server");
		setThreadName("main");

		server::Server server(Url("0.0.0.0", port));
		serverPtr = &server;

		server.start();
		serverPtr = nullptr;
	}
	catch (const std::exception& ex)
	{ logEvent(LogLevel::Error, boost::format("Terminating the application due to an unhandled exception: %1%") % ex.what()); }

	deinitLogger();

	return 0;
}
