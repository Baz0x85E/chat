#pragma one

#include "common/protocol/Listener.hpp"
#include "server/Client.hpp"

namespace server
{

	class Server
	{
	private:
		using Task = std::function<void()>;
		using ClientPtr = std::shared_ptr<Client>;
		using SignalConnections = std::vector<boost::signals2::scoped_connection>;

		struct ClientInfo
		{
			ClientPtr			ptr;
			SignalConnections	connections;

			ClientInfo(const ClientPtr& client, SignalConnections&& clientConnections)
				:	ptr(client),
					connections(std::move(clientConnections))
			{ }
		};

		using UninitialisedClients = std::map<Url, std::pair<ClientInfo, std::chrono::time_point<std::chrono::steady_clock>>>;

		std::map<std::string, ClientInfo>			_clients;

		std::mutex									_uninitialisedClientsMutex;
		UninitialisedClients						_uninitialisedClients;

		std::condition_variable						_tasksCondition;
		std::mutex									_tasksMutex;
		std::queue<Task>							_tasks;

		StopToken									_stopSource;

		protocol::Listener							_listener;

		SignalConnections							_connections;

	public:
		explicit Server(const Url& address);

		void start();
		void requestStop();

	private:
		void addTask(const Task& task);

		template<class Signal, class Handler>
		boost::signals2::connection connectAsTask(Signal& signal, const Handler& handler)
		{
			return signal.connect([this, handler](auto&& ...signalParameters)
			{ addTask(std::bind(handler, std::forward<decltype(signalParameters)>(signalParameters)...)); });
		}

		void onListenerConnectionHandler(const std::shared_ptr<protocol::ServerSideConnection>& connection);
		void onListenerErrorHandler();

		void onInitHandler(const Url& clientAddress, const protocol::ClientMessage::Init& message);
		void onMessageHandler(const std::string& senderName, const protocol::ClientMessage::SendMessage& message);
		void onClientDisconnectedHandler(const std::string& senderName);
	};

}
