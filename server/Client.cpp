#include "server/Client.hpp"

#include "common/Logger.hpp"
#include "common/Thread.hpp"

#include <boost/format.hpp>

namespace server
{

	Client::Client(std::shared_ptr<protocol::ServerSideConnection> connection)
		:	_connection(std::move(connection))
	{
		_connections.push_back(_connection->onRequest().connect(std::bind(&Client::onRequestHandler, this, std::placeholders::_1)));
		_connections.push_back(_connection->onDisconnected().connect([this]{ _onDisconnected(); }));
		_connections.push_back(_connection->onError().connect(std::bind(&Client::onErrorHandler, this)));
	}


	Client::~Client()
	{
		for (const auto& connection : _connections)
			connection.disconnect();

		logEvent(LogLevel::Debug, boost::format("Client::~Client: %1% destroyed") % _name);
		disconnect();
	}


	const std::string& Client::getName() const noexcept
	{ return _name; }


	void Client::enqueueMessage(const std::shared_ptr<protocol::BaseMessage>& message)
	{ _connection->enqueueMessage(message); }


	void Client::disconnect()
	{ _stopSource.stop(); }


	void Client::onRequestHandler(const std::shared_ptr<protocol::Request>& request)
	{
		const protocol::ClientMessage& clientMessage = request->client_message();

		logEvent(LogLevel::Debug,
					boost::format("Client::onRequestHandler: received request of type %1%: %2%")
						% protocol::ClientMessage_MessageType_Name(clientMessage.type())
						% request->DebugString());

		switch (clientMessage.type())
		{
		case protocol::ClientMessage_MessageType_InitType:
		{
			check("Client: request has no ClientMessage::Init", clientMessage.has_init());
			_name = clientMessage.init().name();
			_onInit(clientMessage.init());

			break;
		}

		case protocol::ClientMessage_MessageType_SendMessageType:
		{
			check("Client: request has no ClientMessage::SendMessage", clientMessage.has_send_message());
			_onMessage(clientMessage.send_message());

			break;
		}

		case protocol::ClientMessage_MessageType_DisconnectedType:
			_onDisconnected();
			break;

		default:
			logEvent(LogLevel::Warning, boost::format("Client: unhandled message type: %1%") % protocol::ClientMessage_MessageType_Name(clientMessage.type()));
			break;
		}
	}


	void Client::onErrorHandler()
	{
		disconnect();
		_onDisconnected();
	}

}
