#include "common/Socket.hpp"

#if __has_include(<unistd.h>) // check for posix
	#include "common/posix/Socket.hpp"
#else
	#error Unsupported platform
#endif


std::unique_ptr<ISocket> SocketFactory::create(const Url& endpointAddress)
{
#if __has_include(<unistd.h>)
	return std::make_unique<posix::Socket>(endpointAddress);
#else
	#error Unsupported platform
#endif
}
