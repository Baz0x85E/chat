#pragma once

#include "common/protocol/ConnectionBase.hpp"

namespace protocol
{

	class ServerSideConnection final : public ConnectionBase
	{
	public:
		ServerSideConnection(std::unique_ptr<ISocket> socket, const std::string& threadName);

	protected:
		bool verifyMessage(const Request& request) override;

	private:
		void connect(size_t timeoutSec);
	};

}
