#pragma once

#include "common/protocol/ConnectionBase.hpp"

namespace protocol
{

	class ClientSideConnection final : public ConnectionBase
	{
	public:
		ClientSideConnection(const Url& address, const std::string& threadName);

	protected:
		bool verifyMessage(const Request& request) override;
	};

}
