#include "common/protocol/ServerSideConnection.hpp"

#include "common/Logger.hpp"

namespace protocol
{

	ServerSideConnection::ServerSideConnection(std::unique_ptr<ISocket> socket, const std::string& threadName)
		:	ConnectionBase(std::move(socket), threadName)
	{ }


	bool ServerSideConnection::verifyMessage(const Request& request)
	{
		if (!request.has_client_message())
		{
			logEvent(LogLevel::Error, "ServerSideConnection::verifyMessage: request has no client message");
			return false;
		}

		if (!protocol::ClientMessage_MessageType_IsValid(request.client_message().type()))
		{
			logEvent(LogLevel::Error, "ServerSideConnection::verifyMessage: unknown message type");
			return false;
		}

		return true;
	}

}
