#include "common/protocol/ConnectionBase.hpp"

#include "common/Logger.hpp"
#include "common/Socket.hpp"
#include "common/Thread.hpp"

#include <boost/algorithm/hex.hpp>

#include <google/protobuf/io/coded_stream.h>

namespace protocol
{

	namespace
	{

		constexpr size_t MessageMinSize = sizeof(google::protobuf::uint32);
		constexpr size_t MessageMaxSize = 1024 * 1024;


		ByteArray encodeMessage(const BaseMessage& message)
		{
			const auto messageSize = message.ByteSizeLong();
			check("encodeMessage: message size is zero", messageSize != 0);
			check("encodeMessage: message size is too big", messageSize < MessageMaxSize);

			ByteArray buffer(MessageMinSize + messageSize);

			google::protobuf::io::ArrayOutputStream aos(buffer.data(), buffer.size());
			google::protobuf::io::CodedOutputStream codedOutput(&aos);

			codedOutput.WriteLittleEndian32(static_cast<google::protobuf::uint32>(messageSize));
			message.SerializeToCodedStream(&codedOutput);

			return buffer;
		}


		template<class MessageType>
		std::shared_ptr<MessageType> receiveMessage(ISocket& socket, const IStopToken& token)
		{
			ByteArray messageSizeArray(MessageMinSize);

			if (socket.receive(messageSizeArray, token, false) == 0)
				return nullptr;

			google::protobuf::uint32 messageSize = 0;
			{
				google::protobuf::io::ArrayInputStream ais(messageSizeArray.data(), messageSizeArray.size());
				google::protobuf::io::CodedInputStream codedInput(&ais);
				check("receive: couldn't read message size", codedInput.ReadLittleEndian32(&messageSize));

				logEvent(LogLevel::Debug,
						 boost::format("receive: received size data[%1%]: '%2%' (%3% bytes)") % messageSizeArray.size() % toHex(messageSizeArray) % messageSize);
			}

			check("receive: message size is zero", messageSize != 0);
			check("receive: message size is too big", messageSize < MessageMaxSize);

			ByteArray messageData(messageSize);
			socket.receive(messageData, token, true);

			logEvent(LogLevel::Debug,
					 boost::format("receive: received message data[%1%]: '%2%'") % messageData.size() % toHex(messageData));

			google::protobuf::io::ArrayInputStream ais(messageData.data(), messageData.size());
			google::protobuf::io::CodedInputStream codedInput(&ais);

			auto message = std::make_shared<MessageType>();
			check("receive: couldn't parse message!", message->ParseFromCodedStream(&codedInput));

			return message;
		}


		void sendMessage(const BaseMessage& message, ISocket& socket, const IStopToken& token)
		{
			logEvent(LogLevel::Debug, boost::format("send: trying to send message:\n%1%") % message.GetTypeName());
			const ByteArray data = encodeMessage(message);

			logEvent(LogLevel::Debug, boost::format("send: sending message: data[%1%]: '%2%'") % data.size() % boost::algorithm::hex(std::string(data.begin(), data.end())));
			socket.send(encodeMessage(message), token);
		}


		std::shared_ptr<Request> receiveRequest(ISocket& socket, const IStopToken& token)
		{ return receiveMessage<Request>(socket, token); }

	}


	ConnectionBase::ConnectionBase(const Url& address, const std::string& threadName)
		:	ConnectionBase(SocketFactory::create(address), threadName)
	{ }


	ConnectionBase::ConnectionBase(std::unique_ptr<ISocket> socket, const std::string& threadName)
		:	_socket(std::move(socket)),
			_worker(makeThread(threadName, [this](const IStopToken& token) { threadFunc(token); }))
	{ logEvent(LogLevel::Debug, "ConnectionBase created"); }


	ConnectionBase::~ConnectionBase()
	{
		_worker.stop();
		_socketCondition.notify_one();

		try
		{ disconnect(); }
		catch (const std::exception& ex)
		{ logEvent(LogLevel::Error, boost::format("~ConnectionBase: disconnect error: %1%") % ex.what()); }

		logEvent(LogLevel::Debug, "ConnectionBase destroyed");
	}


	void ConnectionBase::connect(size_t timeoutSec, const IStopToken& token)
	{
		const auto start = std::chrono::steady_clock::now();

		std::lock_guard l(_socketMutex);

		while (!token.isStopped() && !_socket->isConnected() && (std::chrono::steady_clock::now() - start) < std::chrono::seconds(timeoutSec))
		{
			logEvent(LogLevel::Debug, "trying to connect...");

			try
			{
				_socket->connect();
				_socketCondition.notify_one();

				logEvent(LogLevel::Debug, "connected");
			}
			catch (const SocketConnectionRefusedException& ex)
			{
				logEvent(LogLevel::Debug, "couldn't connect");
				std::this_thread::sleep_for(std::chrono::seconds(1));
			}
		}
	}


	void ConnectionBase::disconnect()
	{
		logEvent(LogLevel::Debug, "disconnect");

		std::lock_guard l(_socketMutex);

		_socket->disconnect();
		_socketCondition.notify_one();
		_onDisconnected();
	}


	void ConnectionBase::enqueueMessage(const BaseMessagePtr& message)
	{
		check("enqueueMessage: socket is not connected", _socket->isConnected());

		std::lock_guard l(_messagesMutex);
		_messageQueue.push(message);
	}


	void ConnectionBase::threadFunc(const IStopToken& token) noexcept
	{
		ActionLogger al("threadFunc");

		if (std::unique_lock l(_socketMutex); !_socket->isConnected())
		{
			logEvent(LogLevel::Debug, "awaiting connection...");
			_socketCondition.wait(l, [&token, this] { return token.isStopped() || _socket->isConnected(); });

			if (token.isStopped())
			{
				logEvent(LogLevel::Debug, "stop requested");
				return;
			}

			logEvent(LogLevel::Debug, "connected");
		}

		while (!token.isStopped())
		{
			std::lock_guard l(_socketMutex);

			try
			{
				if (!_socket->isConnected())
					break;

				if (const auto request = receiveRequest(*_socket, token))
				{
					if (verifyMessage(*request))
						_onRequest(request);
					else
						_onError("Couldn't verify message");
				}

				MessageQueue localMessageQueue = [this]
				{
					std::lock_guard l(_messagesMutex);
					return std::exchange(_messageQueue, {});
				}();

				while (!localMessageQueue.empty() && !token.isStopped())
				{
					sendMessage(*localMessageQueue.front(), *_socket, token);
					logEvent(LogLevel::Debug, "message successfuly sent");
					localMessageQueue.pop();
				}
			}
			catch (const SocketConnectionClosedException& ex)
			{
				logEvent(LogLevel::Debug, "connection closed");
				_socket->close();
				_onDisconnected();
			}
			catch (const std::exception& ex)
			{
				logEvent(LogLevel::Error, boost::format("error: %1%") % ex.what());
				_onError(ex.what());
			}
		}
	}

}
