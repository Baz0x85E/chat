#include "common/protocol/Helpers.hpp"

#include "common/CommonTypes.hpp"

namespace protocol
{

	std::shared_ptr<Request> makeClientRequest(ClientMessage_MessageType type)
	{
		check("makeClientRequest: unknown type", ClientMessage_MessageType_IsValid(type));

		std::shared_ptr<Request> request = std::make_shared<Request>();
		request->mutable_client_message()->set_type(type);

		return request;
	}


	std::shared_ptr<Request> makeClientRequestInit(const std::string& clientName)
	{
		auto request = makeClientRequest(ClientMessage_MessageType_InitType);
		request->mutable_client_message()->mutable_init()->set_name(clientName);

		return request;
	}


	std::shared_ptr<Request> makeClientRequestSendMessage(const std::string& message)
	{
		auto request = makeClientRequest(ClientMessage_MessageType_SendMessageType);
		request->mutable_client_message()->mutable_send_message()->set_message(message);

		return request;
	}


	std::shared_ptr<Request> makeServerRequest(ServerMessage_MessageType type)
	{
		check("makeServerRequest: unknown type", ServerMessage_MessageType_IsValid(type));

		std::shared_ptr<Request> request = std::make_shared<Request>();
		request->mutable_server_message()->set_type(type);

		return request;
	}


	std::shared_ptr<Request> makeServerRequestInitResult(bool success, const std::optional<std::string>& description)
	{
		auto request = makeServerRequest(ServerMessage_MessageType_InitResultType);
		auto *const initResult = request->mutable_server_message()->mutable_init_result();

		initResult->set_result(success ? protocol::ServerMessage_InitResult_ResultType_Success : ServerMessage_InitResult_ResultType_Fail);

		if (description && !description->empty())
			initResult->set_description(*description);

		return request;
	}


	std::shared_ptr<Request> makeServerRequestClientSentMessage(const std::string& clientName, const std::string& message, time_t time)
	{
		auto request = makeServerRequest(ServerMessage_MessageType_ClientSentMessageType);
		auto *clientSentMessage = request->mutable_server_message()->mutable_client_sent_message();

		clientSentMessage->set_text(message);
		clientSentMessage->set_name(clientName);
		clientSentMessage->set_time(time);

		return request;
	}


	std::shared_ptr<Request> makeServerRequestNewClient(const std::string& clientName)
	{
		auto request = makeServerRequest(ServerMessage_MessageType_NewClientType);
		request->mutable_server_message()->mutable_new_client()->set_name(clientName);

		return request;
	}


	std::shared_ptr<Request> makeServerRequestClientDisconnected(const std::string& clientName)
	{
		auto request = makeServerRequest(ServerMessage_MessageType_ClientDisconnectedType);
		request->mutable_server_message()->mutable_client_sent_message();

		auto* serverMessage = request->mutable_server_message();
		serverMessage->set_type(protocol::ServerMessage_MessageType_ClientDisconnectedType);
		serverMessage->mutable_client_disconnected()->set_name(clientName);

		return request;
	}

}
