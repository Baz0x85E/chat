#include "common/protocol/Listener.hpp"

#include "common/Logger.hpp"
#include "common/Thread.hpp"

namespace protocol
{

	Listener::Listener(const Url& address)
		:	_socket(SocketFactory::create(address)),
			_worker(makeThread("ListenerWorker", [this](const IStopToken& token) { threadFunc(token); }))
	{ }


	Listener::~Listener()
	{ logEvent(LogLevel::Debug, boost::format("Listener destroyed")); }


	void Listener::threadFunc(const IStopToken& token) noexcept
	{
		ActionLogger al("threadFunc");

		try
		{ _socket->bind(); }
		catch (const std::exception& ex)
		{
			_onError(ex.what());
			return;
		}

		while (!token.isStopped())
		{
			try
			{
				logEvent(LogLevel::Info, "threadFunc: Awaiting a new client... ");

				if (auto clientSocket = _socket->accept(token))
				{
					const auto threadName = boost::format("ServerSideConnection(%1%)") % clientSocket->getEndpointAddress().toString();
					_onConnection(std::make_shared<ServerSideConnection>(std::move(clientSocket), threadName.str()));
				}
			}
			catch (const InterruptedException& ex)
			{
				logEvent(LogLevel::Warning, boost::format("threadFunc: %1%") % ex.what());
				break;
			}
			catch (const std::exception& ex)
			{
				logEvent(LogLevel::Error, boost::format("threadFunc: %1%") % ex.what());
				_onError(ex.what());

				break;
			}
		}
	}

}
