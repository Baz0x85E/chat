#pragma once

#include "common/protocol/CommonTypes.hpp"

#include <optional>

namespace protocol
{

	std::shared_ptr<Request> makeClientRequest(ClientMessage_MessageType type);
	std::shared_ptr<Request> makeClientRequestInit(const std::string& clientName);
	std::shared_ptr<Request> makeClientRequestSendMessage(const std::string& message);

	std::shared_ptr<Request> makeServerRequest(ServerMessage_MessageType type);
	std::shared_ptr<Request> makeServerRequestInitResult(bool success, const std::optional<std::string>& description = std::nullopt);
	std::shared_ptr<Request> makeServerRequestClientSentMessage(const std::string& clientName, const std::string& message, time_t time);
	std::shared_ptr<Request> makeServerRequestNewClient(const std::string& clientName);
	std::shared_ptr<Request> makeServerRequestClientDisconnected(const std::string& clientName);

}

