#pragma once

#include "common/protocol/CommonTypes.hpp"
#include "common/Socket.hpp"
#include "common/Thread.hpp"

#include <boost/signals2.hpp>

#include <condition_variable>
#include <queue>

namespace protocol
{

	class ConnectionBase
	{
	public:
		using OnRequestSignal = boost::signals2::signal<void(const std::shared_ptr<protocol::Request>&)>;
		using OnDisconnectedSignal = boost::signals2::signal<void()>;
		using OnErrorSignal = boost::signals2::signal<void(const std::string&)>;

	private:
		using MessageQueue = std::queue<BaseMessagePtr>;

		std::mutex					_socketMutex;
		std::condition_variable		_socketCondition;
		std::unique_ptr<ISocket>	_socket;

		std::mutex					_messagesMutex;
		MessageQueue				_messageQueue;

		OnRequestSignal				_onRequest;
		OnDisconnectedSignal		_onDisconnected;
		OnErrorSignal				_onError;

		Jthread						_worker;

	protected:
		ConnectionBase(const Url& address, const std::string& threadName);
		ConnectionBase(std::unique_ptr<ISocket> socket, const std::string& threadName);
		virtual ~ConnectionBase();

		virtual bool verifyMessage(const protocol::Request&) = 0;

	public:
		void connect(size_t timeoutSec, const IStopToken& token);
		void disconnect();

		void enqueueMessage(const BaseMessagePtr& message);

		Url getEndpointAddress() const
		{ return _socket->getEndpointAddress(); }

		OnRequestSignal& onRequest() noexcept
		{ return _onRequest; }

		OnDisconnectedSignal& onDisconnected() noexcept
		{ return _onDisconnected; }

		OnErrorSignal& onError() noexcept
		{ return _onError; }

	private:
		void setConnected(bool state);

		void threadFunc(const IStopToken& token) noexcept;
	};

}
