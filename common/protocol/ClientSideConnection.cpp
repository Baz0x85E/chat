#include "common/protocol/ClientSideConnection.hpp"

#include "common/Logger.hpp"

namespace protocol
{

	ClientSideConnection::ClientSideConnection(const Url& address, const std::string& threadName)
		:	ConnectionBase(address, threadName)
	{ }


	bool ClientSideConnection::verifyMessage(const Request& request)
	{
		if (!request.has_server_message())
		{
			logEvent(LogLevel::Error, "ClientSideConnection::verifyMessage: request has no server message");
			return false;
		}

		if (!protocol::ServerMessage_MessageType_IsValid(request.server_message().type()))
		{
			logEvent(LogLevel::Error, "ClientSideConnection::verifyMessage: unknown message type");
			return false;
		}

		return true;
	}

}
