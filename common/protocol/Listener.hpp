#pragma once

#include "common/protocol/ServerSideConnection.hpp"
#include "common/Thread.hpp"

namespace protocol
{

	class Listener final
	{
	public:
		using OnConnectionSignal = boost::signals2::signal<void(const std::shared_ptr<ServerSideConnection>&)>;
		using OnErrorSignal = boost::signals2::signal<void(const std::string& error)>;

	private:
		std::unique_ptr<ISocket>	_socket;

		OnConnectionSignal			_onConnection;
		OnErrorSignal				_onError;

		Jthread						_worker;

	public:
		explicit Listener(const Url& address);
		~Listener();

		OnConnectionSignal& onConnection()
		{ return _onConnection; }

		OnErrorSignal& onError()
		{ return _onError; }

	private:
		void threadFunc(const IStopToken& token) noexcept;
	};

}

