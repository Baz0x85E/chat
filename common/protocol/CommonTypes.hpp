#pragma once

#include "common/Protocol.pb.h"

namespace protocol
{

	using BaseMessage = google::protobuf::MessageLite;
	using BaseMessagePtr = std::shared_ptr<BaseMessage>;

}

