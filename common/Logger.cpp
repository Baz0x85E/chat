#include "common/Logger.hpp"

#include "common/Thread.hpp"

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/shared_ptr.hpp>

#include <iostream>

BOOST_LOG_ATTRIBUTE_KEYWORD(logger_thread_name_attribute, "ThreadName", std::string);

namespace
{

	LogLevel boostSeverityToLogLevel(boost::log::trivial::severity_level level)
	{
		switch (level)
		{
		case boost::log::trivial::severity_level::trace:
		case boost::log::trivial::severity_level::debug:
			return LogLevel::Debug;

		case boost::log::trivial::severity_level::info:
			return LogLevel::Info;

		case boost::log::trivial::severity_level::warning:
			return LogLevel::Warning;

		case boost::log::trivial::severity_level::error:
		case boost::log::trivial::severity_level::fatal:
			return LogLevel::Error;
		}
	}


	boost::log::trivial::severity_level logLevelToBoostSeverity(LogLevel level)
	{
		switch (level)
		{
		case LogLevel::Debug:
			return boost::log::trivial::severity_level::debug;

		case LogLevel::Info:
			return boost::log::trivial::severity_level::info;

		case LogLevel::Warning:
			return boost::log::trivial::severity_level::warning;

		case LogLevel::Error:
			return boost::log::trivial::severity_level::error;
		}
	}


	void formatter(const boost::log::record_view& record, boost::log::formatting_ostream& stream, bool needColors)
	{
		const auto& severity = record[boost::log::trivial::severity];

		if (severity && needColors)
		{
			switch (severity.get())
			{
			case boost::log::trivial::severity_level::info:
				stream << "\033[32m";
				break;

			case boost::log::trivial::severity_level::warning:
				stream << "\033[33m";
				break;

			case boost::log::trivial::severity_level::error:
			case boost::log::trivial::severity_level::fatal:
				stream << "\033[31m";
				break;

			default:
				break;
			}
		}

		boost::log::formatter logFmt = boost::log::expressions::format("[%1%] {%2%} [%3%] %4%")
			% boost::log::expressions::format_date_time<boost::posix_time::ptime>("TimeStamp", "%Y-%m-%d %H:%M:%S")
			% logger_thread_name_attribute
			% boost::log::expressions::attr<boost::log::trivial::severity_level>("Severity")
			% boost::log::expressions::smessage;

		logFmt(record, stream);

		if (severity && needColors)
			stream << "\033[0m";
	}

}


void initLogger(LogLevel logLevel, std::optional<std::string> logFileName)
{
	boost::log::add_common_attributes();
	boost::log::core::get()->set_filter(boost::log::trivial::severity >= logLevelToBoostSeverity(logLevel));

	auto consoleSink = boost::log::add_console_log(std::clog);
	consoleSink->set_formatter(std::bind(formatter, std::placeholders::_1, std::placeholders::_2, true));

	if (logFileName)
	{
		auto fsSink = boost::log::add_file_log(
			boost::log::keywords::file_name = *logFileName + "%1%_%Y-%m-%d_%H-%M-%S.%N.log",
			boost::log::keywords::rotation_size = 10 * 1024 * 1024,
			boost::log::keywords::min_free_space = 30 * 1024 * 1024,
			boost::log::keywords::open_mode = std::ios_base::app);

		fsSink->set_formatter(std::bind(formatter, std::placeholders::_1, std::placeholders::_2, false));
		fsSink->locked_backend()->auto_flush(true);
	}
}


void deinitLogger()
{
	auto core = boost::log::core::get();
	core->flush();
	core->remove_all_sinks();
}


void logEvent(LogLevel level, const std::string& msg) noexcept
{
	try
	{
		switch (level)
		{
		case LogLevel::Debug:
			BOOST_LOG_TRIVIAL(debug) << msg;
			break;

		case LogLevel::Info:
			BOOST_LOG_TRIVIAL(info) << msg;
			break;

		case LogLevel::Warning:
			BOOST_LOG_TRIVIAL(warning) << msg;
			break;

		case LogLevel::Error:
			BOOST_LOG_TRIVIAL(error) << msg;
			break;
		}
	}
	catch (const std::exception& ex)
	{ std::cerr << "Couldn't log message" << std::endl; }
}


void logEvent(LogLevel level, const boost::format& boostFormat) noexcept
{ logEvent(level, boostFormat.str()); }


ActionLogger::ActionLogger(std::string msg, LogLevel level) noexcept
	:	_message(std::move(msg)),
		_level(level)
{ logEvent(level, boost::format("%1% started.") % _message); }


ActionLogger::~ActionLogger()
{ logEvent(_level, boost::format("%1% finished.") % _message); }
