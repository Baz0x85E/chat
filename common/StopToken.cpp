#include "common/StopToken.hpp"

#include <atomic>

namespace
{

	class StopTokenImpl final : public virtual IStopToken
	{
	private:
		std::atomic_bool	_isStopped;

	public:
		explicit StopTokenImpl(bool isStopped)
			:	_isStopped(isStopped)
		{ }

		void stop() noexcept override
		{ _isStopped = true; }

		[[nodiscard]] bool isStopped() const noexcept override
		{ return _isStopped; }
	};

}


StopToken::StopToken(bool isStopped)
	:	_impl(std::make_shared<StopTokenImpl>(isStopped))
{ }
