#pragma once

#include <boost/signals2/connection.hpp>

#include <vector>

using SignalsHolder = std::vector<boost::signals2::scoped_connection>;
