#pragma once

#include "common/StopToken.hpp"

#include <thread>
#include <string>

void setThreadName(const std::string& name) noexcept;
std::string getCurrentThreadName() noexcept;


class Jthread
{
private:
	StopToken	_stopToken{false};
	std::thread	_thread;

public:
	template<class Callable, class... Args>
	Jthread(Callable&& func, Args&&... args)
		:	_thread(std::forward<Callable>(func), _stopToken, std::forward<Args>(args)...)
	{ }

	Jthread(const Jthread&) = delete;
	Jthread(Jthread&&) noexcept = default;

	~Jthread();

	void stop()
	{ _stopToken.stop(); }
};


template<class Callable, class ...Args>
Jthread makeThread(const std::string& name, Callable threadFunc, Args&& ...args)
{
	return Jthread([name, threadFunc, args...](const IStopToken& token)
	{
		setThreadName(name);
		threadFunc(token, std::forward<Args>(args)...);
	});
}
