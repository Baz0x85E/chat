#include "common/Thread.hpp"

#include <boost/log/utility/setup/common_attributes.hpp>

namespace
{

	thread_local std::string ThreadName;

}


void setThreadName(const std::string& name) noexcept
{
	ThreadName = name;
	boost::log::core::get()->add_thread_attribute("ThreadName", boost::log::attributes::constant<std::string>(name));
}


std::string getCurrentThreadName() noexcept
{ return ThreadName; }


Jthread::~Jthread()
{
	stop();
	_thread.join();
}
