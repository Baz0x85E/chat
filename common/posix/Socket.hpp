#pragma once

#include <common/Socket.hpp>

#include <optional>

#include <netinet/in.h>

namespace posix
{

	class Socket final : public virtual ISocket
	{
	private:
		std::optional<int>			_descriptor;
		std::optional<sockaddr_in>	_address = { };
		Url							_endpointAddress;

		bool						_isConnected = false;
		bool						_isClosed = false;

	public:
		explicit Socket(const Url& endpointAddress);
		Socket(int descriptor, const Url& endpointAddress);
		Socket(const Socket& socket) = delete;
		Socket(Socket&& socket) noexcept;
		~Socket() override;

		Socket& operator=(const Socket& socket) = delete;
		Socket& operator=(Socket&& socket) noexcept;

		[[nodiscard]] std::unique_ptr<ISocket> accept(const IStopToken& token) override;
		void bind() override;
		void close() override;

		void connect() override;
		void disconnect() override;
		void reconnect() override;

		[[nodiscard]] bool isConnected() override
		{ return _isConnected; }

		[[nodiscard]] bool isClosed() override
		{ return _isClosed; }

		[[nodiscard]] Url getEndpointAddress() const override
		{ return _endpointAddress; }

		size_t send(const ByteArray& data, const IStopToken& token) override;
		size_t receive(ByteArray& data, const IStopToken& token, bool blocking) override;

	private:
		Socket(int descriptor, const Url& endpointAddress, bool isConnected);

		void ensureConnected();
	};

}
