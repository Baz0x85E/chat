#include "common/posix/Socket.hpp"

#include "common/Logger.hpp"

#include <chrono>
#include <cstring>
#include <iostream>

#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <sys/poll.h>
#include <unistd.h>

using namespace std::literals;


namespace
{

	const size_t BufferSize = 1500;
	const auto SocketTimeout = 5s;

	const int KeepAliveEnabled = 1;
	const int KeepAliveIdleSeconds = 5;
	const int KeepAliveIntervalSeconds = 5;
	const int KeepAliveMaxPackets = 6;


	std::string errnoToString(int err_no)
	{ return "errno: "s + std::to_string(err_no) + ", str: " + strerror(err_no); }


	int createSocket()
	{
		const int descriptor = ::socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

		if (descriptor < 0)
			throw SocketException(errnoToString(errno));

		setsockopt(descriptor, SOL_SOCKET, SO_KEEPALIVE, &KeepAliveEnabled, sizeof(int));
		setsockopt(descriptor, IPPROTO_TCP, TCP_KEEPIDLE, &KeepAliveIdleSeconds, sizeof(int));
		setsockopt(descriptor, IPPROTO_TCP, TCP_KEEPINTVL, &KeepAliveIntervalSeconds, sizeof(int));
		setsockopt(descriptor, IPPROTO_TCP, TCP_KEEPCNT, &KeepAliveMaxPackets, sizeof(int));

		return descriptor;
	}


	void handleErrno()
	{
		switch (errno)
		{
		case EINTR:
			throw InterruptedException(errnoToString(errno));

		case ECONNREFUSED:
			throw SocketConnectionRefusedException(errnoToString(errno));

		case EWOULDBLOCK:
			return;

		case ECONNRESET:
		case EPIPE:
		case ETIMEDOUT:
			throw SocketConnectionClosedException(errnoToString(errno));

		default:
			throw SocketException(errnoToString(errno));
		}
	}

}


namespace posix
{

	Socket::Socket(const Url& endpointAddress)
		:	Socket(createSocket(), endpointAddress, false)
	{ }


	Socket::Socket(int descriptor, const Url& endpointAddress)
		:	Socket(descriptor, endpointAddress, true)
	{ }


	Socket::Socket(Socket&& socket) noexcept
		:	_descriptor(std::exchange(socket._descriptor, std::nullopt)),
			_address(std::exchange(socket._address, std::nullopt)),
			_endpointAddress(std::move(socket._endpointAddress))
	{ }


	Socket::~Socket()
	{
		try
		{ close(); }
		catch (const std::exception& ex)
		{ logEvent(LogLevel::Error, boost::format("Socket::~Socket: error: %1%") % ex.what()); }
	}


	Socket& Socket::operator=(Socket&& socket) noexcept
	{
		_descriptor = std::exchange(socket._descriptor, std::nullopt);
		_address = std::exchange(socket._address, std::nullopt);

		return *this;
	}


	std::unique_ptr<ISocket> Socket::accept(const IStopToken& token)
	{
		check("accept", _descriptor.has_value());

		if (::listen(*_descriptor, 10) != 0)
			throw SocketException(errnoToString(errno));

		fd_set master_set;
		fd_set working_set;
		timeval tv = {.tv_sec = 1, .tv_usec = 0};

		FD_ZERO(&master_set);
		FD_SET(*_descriptor, &master_set);

		while (!token.isStopped())
		{
			memcpy(&working_set, &master_set, sizeof(master_set));
			const int selectResult = ::select(*_descriptor + 1, &working_set, nullptr, nullptr, &tv);

			if (selectResult > 0)
				break;

			if (selectResult == 0)
				continue;

			handleErrno();
		}

		if (token.isStopped())
			return nullptr;

		sockaddr_in address{};
		socklen_t addressSize = sizeof(sockaddr_in);

		if (const int result = accept4(*_descriptor, reinterpret_cast<sockaddr*>(&address), &addressSize, SOCK_CLOEXEC); result > 0)
			return std::make_unique<posix::Socket>(result, Url{inet_ntoa(address.sin_addr), address.sin_port});

		handleErrno();

		return nullptr;
	}


	void Socket::bind()
	{
		check("bind", _descriptor.has_value());

		if (::bind(*_descriptor, reinterpret_cast<const sockaddr*>(&_address), sizeof(_address)) < 0)
			throw SocketException(errnoToString(errno));
	}


	void Socket::close()
	{
		if (_isClosed)
			return;

		logEvent(LogLevel::Debug, boost::format("Socket::close: closing socket with descriptor: %1%")
				% (_descriptor.has_value() ? std::to_string(_descriptor.value()) : "null"));
		check("Socket::close: has no descriptor", _descriptor.has_value());

		if (::shutdown(*_descriptor, SHUT_RDWR) < 0)
			logEvent(LogLevel::Debug, boost::format("Socket::close: couldn't shutdown the socket: %1%") % errnoToString(errno));

		if (::close(*_descriptor) < 0)
			logEvent(LogLevel::Error, boost::format("Socket::close: couldn't close the socket: %1%") % errnoToString(errno));

		_isClosed = true;
		_descriptor = std::nullopt;
		_isConnected = false;
	}


	void Socket::connect()
	{
		check("connect", _descriptor.has_value());
		check("connect", _address.has_value());

		if (::connect(*_descriptor, reinterpret_cast<struct sockaddr*>(&_address), sizeof(_address)) < 0)
		{
			logEvent(LogLevel::Warning, boost::format("Socket::connect: %1%") % errnoToString(errno));
			handleErrno();
		}

		_isConnected = true;
	}


	void Socket::disconnect()
	{ close(); }


	void Socket::reconnect()
	{
		disconnect();
		_descriptor = createSocket();
		connect();
	}


	size_t Socket::send(const ByteArray& data, const IStopToken& token)
	{
		check("Socket::send: data.size() == 0", !data.empty());
		ensureConnected();

		size_t sentBytes = 0;

		do
		{
			const ssize_t result = ::send(*_descriptor, std::next(data.data(), sentBytes), data.size() - sentBytes, MSG_NOSIGNAL);

			if (result > 0)
			{
				sentBytes += result;
				check("Socket::send: sent more than it supposed to be sent", sentBytes <= data.size());

				continue;
			}

			if (result == 0)
			{
				_isConnected = false;
				throw SocketConnectionClosedException("Connection closed");
			}

			try
			{ handleErrno(); }
			catch (const SocketConnectionClosedException& ex)
			{
				_isConnected = false;
				throw;
			}
		}
		while (!token.isStopped() && sentBytes != data.size());

		return sentBytes;
	}


	size_t Socket::receive(ByteArray& data, const IStopToken& token, bool blocking)
	{
		check("Socket::receive: data.size() == 0", !data.empty());
		ensureConnected();

		size_t receivedBytes = 0;

		int flags = MSG_NOSIGNAL;

		if (!blocking)
			flags |= MSG_DONTWAIT;

		do
		{
			const auto result = ::recv(*_descriptor, std::next(data.data(), receivedBytes), data.size() - receivedBytes, flags);

			if (result > 0)
			{
				receivedBytes += result;
				check("Socket::receive: received more than it supposed to be received", receivedBytes <= data.size());

				continue;
			}

			if (result == 0)
			{
				_isConnected = false;
				throw SocketConnectionClosedException("Connection closed");
			}

			try
			{ handleErrno(); }
			catch (const SocketConnectionClosedException& ex)
			{
				_isConnected = false;
				throw;
			}
		}
		while (!token.isStopped() && receivedBytes != data.size() && blocking);

		return receivedBytes;
	}


	Socket::Socket(int descriptor, const Url& endpointAddress, bool isConnected)
		:	_descriptor(descriptor),
			_endpointAddress(endpointAddress),
			_isConnected(isConnected)
	{
		check("descriptor <= 0", descriptor > 0);
		_address = {.sin_family = AF_INET, .sin_port = htons(endpointAddress.port), .sin_addr = {.s_addr = inet_addr(endpointAddress.host.c_str())}};
	}


	void Socket::ensureConnected()
	{
		if (_isClosed)
			throw SocketConnectionClosedException("Connection closed");

		check("Socket::ensureConnected: no descriptor", _descriptor.has_value());
	}

}
