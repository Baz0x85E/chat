#include "common/CommonTypes.hpp"

#include "common/Logger.hpp"

#ifdef DEBUG_MODE
	#undef BACKWARD_HAS_BFD
	#define BACKWARD_HAS_BFD 1
	#include <backward.hpp>
#endif

#include <boost/algorithm/hex.hpp>
#include <boost/format.hpp>

Url::Url(std::string netAddress, uint16_t netPort)
	:	host(std::move(netAddress)),
		port(netPort)
{ }


bool Url::operator<(const Url& other) const noexcept
{
	if (host == other.host)
		return port < other.port;

	return host < other.host;
}


std::string Url::toString() const noexcept
{
	try
	{ return (boost::format("%1%:%2%") % host % port).str(); }
	catch (const std::exception& ex)
	{
		logEvent(LogLevel::Warning, "Url::toString: error");
		return {};
	}
}


class ExceptionWithStacktrace::ExceptionWithStacktraceImpl
{
private:
#ifdef DEBUG_MODE
	backward::StackTrace	_stacktrace;
#endif

public:
	ExceptionWithStacktraceImpl()
	{
#ifdef DEBUG_MODE
		try { _stacktrace.load_here(32); }
		catch (const std::exception& ex)
		{ logEvent(LogLevel::Warning, "Couldn't load stacktrace"); }
#endif
	}

	std::string getStacktrace() noexcept
	{
#ifdef DEBUG_MODE
		try
		{
			std::ostringstream oss;

			backward::Printer printer;
			printer.object = true;
			printer.color_mode = backward::ColorMode::always;
			printer.address = true;
			printer.inliner_context_size = 2;
			printer.trace_context_size = 2;
			printer.print(_stacktrace, oss);

			return oss.str();
		}
		catch (const std::exception& ex)
		{
			logEvent(LogLevel::Warning, "Couldn't create stacktrace");
			return "Couldn't create stacktrace";
		}
#else
		return {};
#endif
	}
};


ExceptionWithStacktrace::ExceptionWithStacktrace(const std::string& msg) noexcept
	:	std::runtime_error(msg),
		_impl(std::make_shared<ExceptionWithStacktraceImpl>())
{ }


std::string ExceptionWithStacktrace::getStacktrace() const noexcept
{ return _impl->getStacktrace(); }


void check(const std::string& msg, bool condition)
{
	if (!condition)
		throw ExceptionWithStacktrace(msg);
}


std::string toHex(const ByteArray& data)
{ return boost::algorithm::hex(std::string(data.begin(), data.end())); }
