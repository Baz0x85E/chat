cmake_minimum_required(VERSION 3.21)

find_package(Protobuf REQUIRED)

if (ANDROID)
	set(Protobuf_PROTOC_EXECUTABLE /usr/bin/protoc)
endif()

protobuf_generate_cpp(PROTO_SRC PROTO_HEADER protocol/Protocol.proto)

if(CMAKE_BUILD_TYPE MATCHES Debug)
	message("Common: debug build")
elseif(CMAKE_BUILD_TYPE MATCHES Release)
	message("Common: release build")
else()
	message(FATAL_ERROR "Error: unsupported build type: ${CMAKE_BUILD_TYPE}")
endif()

if(${STATIC_BUILD})
	message("Common: Static build")
	set(LinkageType "STATIC")
else()
	message("Common: Shared build")
	set(LinkageType "SHARED")
endif()

add_library(common ${LinkageType}
	"posix/Socket.cpp"
	"protocol/ClientSideConnection.cpp"
	"protocol/ConnectionBase.cpp"
	"protocol/Helpers.cpp"
	"protocol/Listener.cpp"
	"protocol/ServerSideConnection.cpp"
	"CommonTypes.cpp"
	"Logger.cpp"
	"Socket.cpp"
	"StopToken.cpp"
	"Thread.cpp"
	${PROTO_SRC}
	${PROTO_HEADER}
)

target_include_directories(common PRIVATE ${CMAKE_BINARY_DIR})
