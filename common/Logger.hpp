#pragma once

#include <boost/format.hpp>

#include <string>
#include <optional>

enum class LogLevel
{
	Debug,
	Info,
	Warning,
	Error
};


void initLogger(LogLevel logLevel, std::optional<std::string> logFileName = std::nullopt);
void deinitLogger();


void logEvent(LogLevel level, const std::string& msg) noexcept;
void logEvent(LogLevel level, const boost::format& boostFormat) noexcept;


class ActionLogger final
{
private:
	const std::string	_message;
	const LogLevel		_level;

public:
	ActionLogger(std::string msg, LogLevel level = LogLevel::Debug) noexcept;
	~ActionLogger();
};
