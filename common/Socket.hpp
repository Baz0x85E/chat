#pragma once

#include "common/CommonTypes.hpp"
#include "common/StopToken.hpp"

class SocketException : public ExceptionWithStacktrace
{
public:
	explicit SocketException(const std::string& msg) noexcept
		:	ExceptionWithStacktrace(msg)
	{ }
};


class SocketConnectionClosedException : public SocketException
{
public:
	explicit SocketConnectionClosedException(const std::string& msg) noexcept
		:	SocketException(msg)
	{ }
};


class SocketConnectionRefusedException : public SocketException
{
public:
	explicit SocketConnectionRefusedException(const std::string& msg) noexcept
		:	SocketException(msg)
	{ }
};


class SocketBrokenPipeException : public SocketException
{
public:
	explicit SocketBrokenPipeException(const std::string& msg) noexcept
		:	SocketException(msg)
	{ }
};


class SocketTimeoutException : public SocketException
{
public:
	explicit SocketTimeoutException(const std::string& msg) noexcept
		:	SocketException(msg)
	{ }
};


struct ISocket
{
public:
	virtual ~ISocket() = default;

	[[nodiscard]] virtual std::unique_ptr<ISocket> accept(const IStopToken& token) = 0;
	virtual void bind() = 0;
	virtual void close() = 0;

	virtual void connect() = 0;
	virtual void disconnect() = 0;
	virtual void reconnect() = 0;

	[[nodiscard]] virtual bool isConnected() = 0;
	[[nodiscard]] virtual bool isClosed() = 0;

	[[nodiscard]] virtual Url getEndpointAddress() const = 0;

	virtual size_t send(const ByteArray& data, const IStopToken& token) = 0;
	virtual size_t receive(ByteArray& data, const IStopToken& token, bool blocking) = 0;
};


struct SocketFactory final
{
	[[nodiscard]] static std::unique_ptr<ISocket> create(const Url& endpointAddress);
};
