#pragma once

#include <chrono>
#include <stdexcept>
#include <vector>
#include <memory>

using ByteArray = std::vector<char>;

struct Url
{
	std::string	host;
	uint16_t	port;

	Url(std::string netAddress, uint16_t netPort);

	bool operator<(const Url& other) const noexcept;

	std::string toString() const noexcept;
};


class ExceptionWithStacktrace : public std::runtime_error
{
private:
	class ExceptionWithStacktraceImpl;
	const std::shared_ptr<ExceptionWithStacktraceImpl>	_impl;

public:
	explicit ExceptionWithStacktrace(const std::string& msg) noexcept;

	std::string getStacktrace() const noexcept;
};


class InterruptedException : public std::runtime_error
{
public:
	explicit InterruptedException(const std::string& msg) noexcept
		:	std::runtime_error(msg)
	{ }
};


void check(const std::string& msg, bool condition);
std::string toHex(const ByteArray& data);
