#pragma once

#include <memory>

struct IStopToken
{
	virtual ~IStopToken() = default;

	virtual void stop() noexcept = 0;
	[[nodiscard]] virtual bool isStopped() const noexcept = 0;
};


class StopToken : public virtual IStopToken
{
private:
	const std::shared_ptr<IStopToken>	_impl;

public:
	explicit StopToken(bool isStopped = false);

	void stop() noexcept override
	{ _impl->stop(); }

	[[nodiscard]] bool isStopped() const noexcept override
	{ return _impl->isStopped(); }
};
