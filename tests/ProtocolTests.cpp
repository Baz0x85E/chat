#define BOOST_TEST_MODULE Tests
#include <boost/test/unit_test.hpp>

#include "common/protocol/ClientSideConnection.hpp"
#include "common/protocol/Helpers.hpp"
#include "common/protocol/Listener.hpp"
#include "common/protocol/ServerSideConnection.hpp"

#include "common/Logger.hpp"
#include "common/Thread.hpp"

using namespace boost::unit_test;


namespace
{

	const Url Address = {"0.0.0.0", 8081};

}


BOOST_AUTO_TEST_CASE(ProtocolLogInitTest)
{
	initLogger(LogLevel::Debug, "tests");
	setThreadName("ProtocolTests");
}


BOOST_AUTO_TEST_CASE(ProtocolListenerDestructionTest)
{
	ActionLogger al("ProtocolListenerDestructionTest");

	{
		const auto onListenerErrorHandler = [&](const std::string& error)
		{ BOOST_FAIL("ListenerError"); };

		protocol::Listener listener(Address);
		listener.onError().connect(onListenerErrorHandler);
	}
}


BOOST_AUTO_TEST_CASE(ProtocolClientSideConnectionDestructionTest)
{
	ActionLogger al("ProtocolClientSideConnectionDestructionTest");

	{
		protocol::ClientSideConnection clientConnection(Address, "ClientSideConnection");
		clientConnection.onError().connect([&](const std::string& error) { BOOST_FAIL("ConnectionErrorHandler"); });
	}
}


BOOST_AUTO_TEST_CASE(ProtocolServerSideConnectionDestructionTest)
{
	ActionLogger al("ProtocolServerSideConnectionDestructionTest");

	{
		protocol::ServerSideConnection serverConnection(SocketFactory::create(Address), "ServerSideConnection");
		serverConnection.onError().connect([&](const std::string& error) { BOOST_FAIL("ConnectionErrorHandler"); });
	}
}


BOOST_AUTO_TEST_CASE(ProtocolListenerConnectionTest)
{
	ActionLogger al("ProtocolListenerConnectionTest");

	{
		protocol::Listener listener(Address);
		listener.onError().connect([](const std::string& error) { BOOST_FAIL("ListenerError"); });

		std::condition_variable clientConnectionCondition;
		std::condition_variable clientDisconnectedCondition;
		protocol::ClientSideConnection clientConnection(Address, "ClientSideConnection");
		clientConnection.onError().connect([&](const std::string& error) { BOOST_FAIL("ConnectionErrorHandler"); });
		bool clientDisconnected = false;

		std::condition_variable serverConnectionCondition;
		std::shared_ptr<protocol::ServerSideConnection> serverConnection;
		bool serverDisconnected = false;

		BOOST_TEST_CHECKPOINT("listener.onConnection()");
		listener.onConnection().connect([&](const std::shared_ptr<protocol::ServerSideConnection>& connection)
		{
			serverConnection = connection;
			serverConnection->onDisconnected().connect([&serverDisconnected, &serverConnectionCondition]
			{
				serverDisconnected = true;
				serverConnectionCondition.notify_one();
			});

			serverConnectionCondition.notify_one();
		});

		std::mutex mutex;

		BOOST_TEST_CHECKPOINT("clientConnection.connect(5)");
		{
			std::unique_lock l(mutex);

			clientConnection.connect(5, StopToken());
			serverConnectionCondition.wait_for(l, std::chrono::seconds(10), [&serverConnection] { return serverConnection != nullptr; });

			BOOST_REQUIRE(serverConnection != nullptr);
		}

		BOOST_TEST_CHECKPOINT("clientConnection.onDisconnected()");
		{
			clientConnection.onDisconnected().connect([&clientDisconnected, &clientDisconnectedCondition]
			{
				std::cerr << "clientConnection.onDisconnected" << std::endl;
				clientDisconnected = true;
				clientDisconnectedCondition.notify_one();
			});

			std::unique_lock l(mutex);

			BOOST_TEST_CHECKPOINT("clientConnection.disconnect");
			clientConnection.disconnect();
			clientDisconnectedCondition.wait_for(l, std::chrono::seconds(5), [&clientDisconnected] { return clientDisconnected; });
			BOOST_REQUIRE(clientDisconnected);
			BOOST_CHECK(clientDisconnected);
		}

		{
			std::unique_lock l(mutex);

			BOOST_TEST_CHECKPOINT("serverConnection->disconnect()");
			serverConnection->disconnect();
			serverConnectionCondition.wait_for(l, std::chrono::seconds(5), [&serverDisconnected] { return serverDisconnected; });
			BOOST_REQUIRE(serverDisconnected);
		}
	}
}


BOOST_AUTO_TEST_CASE(ProtocolRequestTest)
{
	ActionLogger al("ProtocolRequestTest");

	std::mutex mutex;

	const auto onListenerErrorHandler = [&](const std::string& error)
	{ BOOST_FAIL("ListenerError"); };

	const auto onConnectionErrorHandler = [&](const std::string& error)
	{ BOOST_FAIL("ConnectionErrorHandler"); };

	protocol::Listener listener(Address);
	listener.onError().connect(onListenerErrorHandler);

	std::condition_variable clientConnectionCondition;
	protocol::ClientSideConnection clientConnection(Address, "ClientSideConnection");
	clientConnection.onError().connect(onConnectionErrorHandler);

	std::condition_variable serverConnectionCondition;
	std::shared_ptr<protocol::ServerSideConnection> serverConnection;
	bool serverDisconnected = false;

	std::condition_variable requestCondition;
	std::shared_ptr<protocol::Request> request;

	std::condition_variable clientDisconnectedCondition;
	bool clientDisconnected = false;

	clientConnection.onDisconnected().connect([&clientDisconnected, &clientDisconnectedCondition]
	{
		clientDisconnected = true;
		clientDisconnectedCondition.notify_one();
	});

	const auto onRequestHandler = [&request, &requestCondition](const std::shared_ptr<protocol::Request>& rq)
	{
		request = rq;
		requestCondition.notify_one();
	};

	listener.onConnection().connect([&](const std::shared_ptr<protocol::ServerSideConnection>& connection)
	{
		serverConnection = connection;
		serverConnection->onRequest().connect(onRequestHandler);
		serverConnection->onDisconnected().connect([&serverDisconnected, &serverConnectionCondition]
		{
			serverDisconnected = true;
			serverConnectionCondition.notify_one();
		});

		serverConnectionCondition.notify_one();
	});

	{
		std::unique_lock l(mutex);

		clientConnection.connect(5, StopToken());
		serverConnectionCondition.wait_for(l, std::chrono::seconds(10), [&serverConnection] { return serverConnection != nullptr; });

		BOOST_REQUIRE(serverConnection != nullptr);
	}

	{
		std::unique_lock l(mutex);

		clientConnection.enqueueMessage(protocol::makeClientRequestInit("nickita"));
		requestCondition.wait_for(l, std::chrono::seconds(5), [&request] { return request != nullptr; });
		BOOST_REQUIRE(request != nullptr);
	}

	{
		std::unique_lock l(mutex);

		clientConnection.disconnect();
		clientDisconnectedCondition.wait_for(l, std::chrono::seconds(5), [&clientDisconnected] { return clientDisconnected; });
		BOOST_REQUIRE(clientDisconnected);
	}

	{
		std::unique_lock l(mutex);

		serverConnection->disconnect();
		serverConnectionCondition.wait_for(l, std::chrono::seconds(5), [&serverDisconnected] { return serverDisconnected; });
		BOOST_REQUIRE(serverDisconnected);
	}
}

//BOOST_AUTO_TEST_CASE( my_test )
//{
//	// seven ways to detect and report the same error:
//	BOOST_CHECK( add( 2,2 ) == 4 );        // #1 continues on error

//	BOOST_REQUIRE( add( 2,2 ) == 4 );      // #2 throws on error

//	if( add( 2,2 ) != 4 )
//	  BOOST_ERROR( "Ouch..." );            // #3 continues on error

//	if( add( 2,2 ) != 4 )
//	  BOOST_FAIL( "Ouch..." );             // #4 throws on error

//	if( add( 2,2 ) != 4 ) throw "Ouch..."; // #5 throws on error

//	BOOST_CHECK_MESSAGE( add( 2,2 ) == 4,  // #6 continues on error
//						 "add(..) result: " << add( 2,2 ) );

//	BOOST_CHECK_EQUAL( add( 2,2 ), 4 );	  // #7 continues on error
//}


//BOOST_AUTO_TEST_CASE( force_division_by_zero )
//{
//	BOOST_CHECK( false );

//	// unit test framework can catch operating system signals
//	BOOST_TEST_CHECKPOINT("About to force division by zero!");
//	int i = 1, j = 0;

//	// reports 'unknown location(0): fatal error in "force_division_by_zero": integer divide by zero'
//	i = i / j;
//}
