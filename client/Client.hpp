#pragma once

#include "common/protocol/ClientSideConnection.hpp"

#include <optional>

namespace client
{

	class Client
	{
	public:
		struct Signals
		{
			const std::reference_wrapper<protocol::ConnectionBase::OnRequestSignal>			onRequest;
			const std::reference_wrapper<protocol::ConnectionBase::OnDisconnectedSignal>	onDisconnected;
			const std::reference_wrapper<protocol::ConnectionBase::OnErrorSignal>			onError;
		};

	private:
		std::string						_name;

		protocol::ClientSideConnection	_connection;

	public:
		explicit Client(std::string name, const Url& address);

		std::optional<Signals> connect(size_t timeoutSec, const IStopToken& token);
		void sendMessage(const std::string& message);
	};

}
