#pragma once

#include <boost/format.hpp>

namespace client
{

	std::string underlineText(const std::string& text);
	std::string boldText(const std::string& text);

	void printMessage(const boost::format& format) noexcept;
	void printMessage(const std::string& msg) noexcept;

}