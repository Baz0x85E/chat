#include "client/console/ChatLogger.hpp"
#include "client/Client.hpp"
#include "common/Logger.hpp"
#include "common/Signals.hpp"
#include "common/Thread.hpp"

#ifdef DEBUG_MODE
	#undef BACKWARD_HAS_BFD
	#define BACKWARD_HAS_BFD 1
	#include <backward.hpp>
#endif

#include <boost/format.hpp>

#include <csignal>
#include <iostream>

namespace
{
#ifdef DEBUG_MODE
	backward::SignalHandling sh;
#endif

	StopToken stopSource;


	void sigtermHandler(int)
	{ stopSource.stop(); }


	void requestHandler(const std::shared_ptr<protocol::Request>& request)
	{
		switch (request->server_message().type())
		{
		case protocol::ServerMessage_MessageType::ServerMessage_MessageType_ClientDisconnectedType:
			client::printMessage(boost::format("%1% disconnected") % client::underlineText(request->server_message().client_disconnected().name()));
			break;

		case protocol::ServerMessage_MessageType::ServerMessage_MessageType_ClientSentMessageType:
			client::printMessage(boost::format("%1%: %2%")
					% client::underlineText(request->server_message().client_sent_message().name())
					% request->server_message().client_sent_message().text()
			);
			break;

		case protocol::ServerMessage_MessageType::ServerMessage_MessageType_InitResultType:
			break;

		case protocol::ServerMessage_MessageType::ServerMessage_MessageType_NewClientType:
			client::printMessage(boost::format("%1% connected") % client::underlineText(request->server_message().new_client().name()));
			break;

		case protocol::ServerMessage_MessageType::ServerMessage_MessageType_ServerMessage_MessageType_INT_MAX_SENTINEL_DO_NOT_USE_:
		case protocol::ServerMessage_MessageType::ServerMessage_MessageType_ServerMessage_MessageType_INT_MIN_SENTINEL_DO_NOT_USE_:
			break;
		}
	}

}


int main(int argc, char *argv[])
{
	LogLevel logLevel = LogLevel::Error;

	if (argc > 1 && std::string(argv[1]) == "debug")
		logLevel = LogLevel::Debug;

	std::signal(SIGTERM, sigtermHandler);
	std::signal(SIGINT, sigtermHandler);

	initLogger(logLevel, "client");
	setThreadName("main");

	std::string clientName = "unnamed";
	std::cout << "Enter your nickname: ";
	std::cin >> clientName;

	std::string ip = "0.0.0.0";
	std::string port = "8091";

	std::cout << "Enter server's hostname: ";
	std::cin >> ip;

	std::cout << "Enter server's port: ";
	std::cin >> port;

	std::mutex connectionsMutex;
	SignalsHolder connections;

	try
	{
		const Url serverAddress(ip, std::stoul(port));
		client::Client client(clientName, serverAddress);

		client::printMessage(boost::format("Trying to connect to the server(%1%)") % serverAddress.toString());

		if (auto signals = client.connect(30, stopSource))
		{
			std::lock_guard l(connectionsMutex);

			connections.emplace_back(signals->onRequest.get().connect(requestHandler));

			connections.emplace_back(signals->onError.get().connect([&](const std::string& error)
			{
				client::printMessage(boost::format("Connection error occured while connecting to the server: %1%") % error);

				stopSource.stop();

				std::lock_guard l(connectionsMutex);
				connections.clear();
			}));

			connections.emplace_back(signals->onDisconnected.get().connect([&]
			{
				client::printMessage("Server disconnected");

				std::lock_guard l(connectionsMutex);
				connections.clear();
			}));
		}
		else
			stopSource.stop();

		while (!stopSource.isStopped())
		{
			std::string message;
			std::getline(std::cin, message);

			if (!stopSource.isStopped() && !message.empty())
				client.sendMessage(message);
		}
	}
	catch (const std::exception& ex)
	{
		logEvent(LogLevel::Error, boost::format("Terminating the application due to an unhandled exception: %1%") % ex.what());

		stopSource.stop();

		std::lock_guard l(connectionsMutex);
		connections.clear();
	}

	deinitLogger();
	client::printMessage("Finished");

	return 0;
}
