#include "client/console/ChatLogger.hpp"

#include <chrono>
#include <iomanip>
#include <iostream>

namespace client
{

	std::string underlineText(const std::string& text)
	{ return (boost::format("\033[4m%1%\033[0m") % text).str(); }


	std::string boldText(const std::string& text)
	{ return (boost::format("\x1b[1m%1%\x1b[0m") % text).str(); }


	void printMessage(const boost::format& format) noexcept
	{ printMessage(format.str()); }


	void printMessage(const std::string& msg) noexcept
	{
		const auto time = std::time(nullptr);
		const auto* localTime = std::localtime(&time);

		std::cout
			<< std::put_time(localTime, boldText("[%H:%M] ").c_str())
			<< msg
			<< std::endl;
	}

}
