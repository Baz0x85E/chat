#include "client/Client.hpp"

#include "client/console/ChatLogger.hpp"
#include "common/protocol/Helpers.hpp"
#include "common/Signals.hpp"

namespace client
{

	Client::Client(std::string name, const Url& address)
		:	_name(std::move(name)),
			_connection(address, "Connection")
	{ }


	std::optional<Client::Signals> Client::connect(size_t timeoutSec, const IStopToken& token)
	{
		bool isInitSuccessful = false;
		bool hasError = false;
		bool isDisconnected = false;

		std::condition_variable eventCondition;
		std::mutex mutex;

		SignalsHolder signalConnections;
		signalConnections.emplace_back(_connection.onRequest().connect([&](const std::shared_ptr<protocol::Request>& request)
		{
			if (request->server_message().has_init_result())
			{
				isInitSuccessful = request->server_message().init_result().result() == protocol::ServerMessage_InitResult_ResultType_Success;
				printMessage(boost::format("Connected. Server message: %1%") % request->server_message().init_result().description());
			}
			else
				hasError = true;

			eventCondition.notify_one();
		}));

		signalConnections.emplace_back(_connection.onError().connect([&](const std::string& error)
		{
			hasError = true;
			printMessage(boost::format("Connection error: %1%") % error);

			eventCondition.notify_one();
		}));

		signalConnections.emplace_back(_connection.onDisconnected().connect([&]
		{
			isDisconnected = true;
			printMessage("Client::connect: Server disconnected");

			eventCondition.notify_one();
		}));

		try
		{
			_connection.connect(timeoutSec, token);
			_connection.enqueueMessage(protocol::makeClientRequestInit(_name));
		}
		catch (const std::exception& ex)
		{
			printMessage(boost::format("Client::connect: couldn't connect due to: %1%") % ex.what());
			signalConnections.clear();

			return std::nullopt;
		}

		{
			std::unique_lock l(mutex);
			eventCondition.wait(l);
		}

		signalConnections.clear();

		if (!isInitSuccessful || hasError || isDisconnected)
			return std::nullopt;

		return Signals{_connection.onRequest(), _connection.onDisconnected(), _connection.onError()};
	}


	void Client::sendMessage(const std::string& message)
	{ _connection.enqueueMessage(protocol::makeClientRequestSendMessage(message)); }

}
